package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"
)

// group information
type group struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type groupResult struct {
	OK       bool      `json:"ok"`
	Error    string    `json:"error,omitempty"`
	Groups   []group   `json:"groups,omitempty"`
	Messages []message `json:"messages,omitempty"`
}

// GetGroups to get all the groups information
func GetGroups(token string) error {
	url := fmt.Sprintf("https://slack.com/api/groups.list?token=%s", token)
	resp, err := newCli().Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("bad request")
	}

	var rs groupResult
	if err := json.NewDecoder(resp.Body).Decode(&rs); err != nil {
		// decode the json
		return err
	}

	if !rs.OK {
		// request not OK
		return errors.New(rs.Error)
	}

	for _, one := range rs.Groups {
		println("id:", one.ID, " name:", one.Name)
	}

	return nil
}

// GroupHistory to get the history message for a group
func GroupHistory(id, token, toFile, oldest string, pretty bool) error {
	var url string

	if oldest == "" {
		url = fmt.Sprintf("https://slack.com/api/groups.history?channel=%s&token=%s&count=1000", id, token)
	} else {
		url = fmt.Sprintf("https://slack.com/api/groups.history?channel=%s&token=%s&count=1000&oldest=%s", id, token, oldest)
	}

	fmt.Println("url:", url)

	resp, err := newCli().Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("bad request")
	}

	var rs groupResult
	if err := json.NewDecoder(resp.Body).Decode(&rs); err != nil {
		// decode the json
		return err
	}
	if !rs.OK {
		// request not OK
		return errors.New(rs.Error)
	}

	var f *os.File
	if toFile != "" {
		var err error
		f, err = os.OpenFile(toFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}

		defer f.Close()
	}

	count := 0
	for _, one := range rs.Messages {
		tsText := one.TS
		if pretty {
			value, err := strconv.ParseFloat(tsText, 64)
			if err != nil {
				fmt.Println("parsing error:", err.Error())
				continue
			}
			tm := time.Unix(int64(value), 0)
			tsText = tm.Format("2006-01-02 15:04:05")
		}

		if f == nil {
			println("ts:", tsText, " text:", one.Text)
		} else {
			text := fmt.Sprintf("%s,%s\n", tsText, one.Text)
			if _, err := f.WriteString(text); err != nil {
				return err
			}
		}
		count++
	}
	println("total:", count)

	return nil
}
