package main

import (
	"fmt"
	"os"

	"bitbucket.org/kkhku/slackfias/cmd"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "fias"
	app.Usage = "Some commands to export FIAS data from Slack"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "token, t",
			Usage: "The token generated on Slack.",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:  "channels",
			Usage: "get all channels",
			Action: func(c *cli.Context) error {
				err := cmd.GetChannels(c.GlobalString("token"))
				if err != nil {
					fmt.Println(err.Error())
				}
				return err
			},
		},
		{
			Name:  "channelhistory",
			Usage: "get messages of a channel",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "id",
					Usage: "The id of a channel",
				},
				cli.StringFlag{
					Name:  "tofile, f",
					Usage: "the file name needed to be exported to",
				},
				cli.StringFlag{
					Name:  "oldest",
					Usage: "the oldest timestamp",
				},
				cli.BoolFlag{
					Name:  "pretty",
					Usage: "to print out pretty text",
				},
			},
			Action: func(c *cli.Context) error {
				err := cmd.ChannelHistory(c.String("id"), (c.GlobalString("token")), c.String("tofile"), c.String("oldest"), c.Bool("pretty"))
				if err != nil {
					fmt.Println(err.Error())
				}
				return err
			},
		},
		{
			Name:  "groups",
			Usage: "get all groups",
			Action: func(c *cli.Context) error {
				err := cmd.GetGroups(c.GlobalString("token"))
				if err != nil {
					fmt.Println(err.Error())
				}
				return err
			},
		},
		{
			Name:  "grouphistory",
			Usage: "get messages of a group",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "id",
					Usage: "The id of a group",
				},
				cli.StringFlag{
					Name:  "tofile, f",
					Usage: "the file name needed to be exported to",
				},
				cli.StringFlag{
					Name:  "oldest",
					Usage: "the oldest timestamp",
				},
				cli.BoolFlag{
					Name:  "pretty",
					Usage: "to print out pretty text",
				},
			},
			Action: func(c *cli.Context) error {
				err := cmd.GroupHistory(c.String("id"), (c.GlobalString("token")), c.String("tofile"), c.String("oldest"), c.Bool("pretty"))
				if err != nil {
					fmt.Println(err.Error())
				}
				return err
			},
		},
		{
			Name:  "ims",
			Usage: "get all ims",
			Action: func(c *cli.Context) error {
				err := cmd.GetIMs(c.GlobalString("token"))
				if err != nil {
					fmt.Println(err.Error())
				}
				return err
			},
		},
		{
			Name:  "imhistory",
			Usage: "get messages of an IM",
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "id",
					Usage: "The id of a group",
				},
				cli.StringFlag{
					Name:  "tofile, f",
					Usage: "the file name needed to be exported to",
				},
				cli.StringFlag{
					Name:  "oldest",
					Usage: "the oldest timestamp",
				},
				cli.BoolFlag{
					Name:  "pretty",
					Usage: "to print out pretty text",
				},
			},
			Action: func(c *cli.Context) error {
				err := cmd.IMHistory(c.String("id"), (c.GlobalString("token")), c.String("tofile"), c.String("oldest"), c.Bool("pretty"))
				if err != nil {
					fmt.Println(err.Error())
				}
				return err
			},
		},
	}

	app.Run(os.Args)
}
